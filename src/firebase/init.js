import firebase from 'firebase'
// eslint-disable-next-line no-unused-vars
import firestore from 'firebase/firestore'

var firebaseConfig = {
  apiKey: 'AIzaSyCHig_E8N_InOZtCTxs2FkkatnSG-t-cGM',
  authDomain: 'ninja-smoothies-83771.firebaseapp.com',
  databaseURL: 'https://ninja-smoothies-83771.firebaseio.com',
  projectId: 'ninja-smoothies-83771',
  storageBucket: 'ninja-smoothies-83771.appspot.com',
  messagingSenderId: '652262508623',
  appId: '1:652262508623:web:bd9ff619eb3b89a793c23a',
  measurementId: 'G-22FBNYPT4X'
}

// eslint-disable-next-line no-undef,no-unused-vars
const firebaseApp = firebase.initializeApp(firebaseConfig)
firebaseApp.firestore().settings({ timestampsInSnapshots: true })

export default firebaseApp.firestore()
